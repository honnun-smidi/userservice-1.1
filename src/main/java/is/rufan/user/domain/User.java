package is.rufan.user.domain;

import is.rufan.team.domain.Team;

public class User
{
  protected int id;
  protected String name;
  protected String username;
  protected String email;
  protected String password;
  protected Team favoriteTeam;
  protected String creditCardNumber;
  protected CreditCardType creditCardType;
  protected int creditCardExpMonth;
  protected int creditCardExpYear;

  public User()

  {
  }

  public User(int id, String name, String username, String email, String password, Team favoriteTeam, String creditCardNumber,
              CreditCardType creditCardType, int creditCardExpMonth, int creditCardExpYear) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
    this.password = password;
    this.favoriteTeam = favoriteTeam;
    this.creditCardNumber = creditCardNumber;
    this.creditCardType = creditCardType;
    this.creditCardExpMonth = creditCardExpMonth;
    this.creditCardExpYear = creditCardExpYear;
  }

  public User(String name, String username, String email, String password, Team favoriteTeam, String creditCardNumber,
              CreditCardType creditCardType, int creditCardExpMonth, int creditCardExpYear) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.password = password;
    this.favoriteTeam = favoriteTeam;
    this.creditCardNumber = creditCardNumber;
    this.creditCardType = creditCardType;
    this.creditCardExpMonth = creditCardExpMonth;
    this.creditCardExpYear = creditCardExpYear;
  }

  public int getId()
  {
    return id;
  }

  public void setId(int id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getEmail()
  {
    return email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public Team getFavoriteTeam() {
    return favoriteTeam;
  }

  public void setFavoriteTeam(Team favoriteTeam) {
    this.favoriteTeam = favoriteTeam;
  }

  public String getCreditCardNumber() {
    return creditCardNumber;
  }

  public void setCreditCardNumber(String creditCardNumber) {
    this.creditCardNumber = creditCardNumber;
  }

  public CreditCardType getCreditCardType() {
    return creditCardType;
  }

  public void setCreditCardType(CreditCardType creditCardType) {
    this.creditCardType = creditCardType;
  }

  public int getCreditCardExpMonth() {
    return creditCardExpMonth;
  }

  public void setCreditCardExpMonth(int creditCardExpMonth) {
    this.creditCardExpMonth = creditCardExpMonth;
  }

  public int getCreditCardExpYear() {
    return creditCardExpYear;
  }

  public void setCreditCardExpYear(int creditCardExpYear) {
    this.creditCardExpYear = creditCardExpYear;
  }

  @Override
  public String toString() {
    return "User{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", username='" + username + '\'' +
            ", email='" + email + '\'' +
            ", password='" + password + '\'' +
            ", favoriteTeam=" + favoriteTeam +
            ", creditCardNumber='" + creditCardNumber + '\'' +
            ", creditCardType=" + creditCardType +
            ", creditCardExpMonth=" + creditCardExpMonth +
            ", creditCardExpYear=" + creditCardExpYear +
            '}';
  }
}
