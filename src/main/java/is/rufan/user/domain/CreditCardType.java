package is.rufan.user.domain;

/**
 * Created by Omar on 26.10.2015.
 */
public enum CreditCardType {
    MASTERCARD,
    VISA;

    public static CreditCardType fromInt(int x) {
        switch (x) {
            case 0:
                return MASTERCARD;
            case 1:
                return VISA;

        }
        return null;
    }
}

