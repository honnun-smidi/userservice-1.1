package is.rufan.user.data;

import java.sql.ResultSet;
import java.sql.SQLException;

import is.rufan.team.domain.Team;
import is.rufan.user.domain.CreditCardType;
import is.rufan.user.domain.User;
import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<User>
{
  public User mapRow(ResultSet rs, int rowNum) throws SQLException
  {
    User user = new User();
    user.setId(rs.getInt("id"));
    user.setName(rs.getString("name"));
    user.setUsername(rs.getString("username"));
    user.setEmail(rs.getString("email"));
    user.setPassword(rs.getString("password"));
    user.setCreditCardNumber(rs.getString("creditCardNumber"));
    user.setCreditCardType(CreditCardType.fromInt(rs.getInt("creditCardType")));
    user.setCreditCardExpMonth(rs.getInt("creditCardExpMonth"));
    user.setCreditCardExpYear(rs.getInt("creditCardExpYear"));
    // TODO: set favorite team!
    int favTeamId = rs.getInt("teamid");
    if (favTeamId != 0) {
      Team favTeam = new Team();
      favTeam.setTeamId(favTeamId);
      favTeam.setLocation(rs.getString("location"));
      favTeam.setAbbreviation(rs.getString("abbreviation"));
      favTeam.setDisplayName(rs.getString("displayname"));
      // the team's venue is discarded for this purpose
      user.setFavoriteTeam(favTeam);
    } else
      user.setFavoriteTeam(null);

    return user;
  }
}

