package is.rufan.user.data;

import is.rufan.user.domain.User;
import is.ruframework.data.RuDataAccess;

public interface UserDataGateway extends RuDataAccess
{
  int create(User user);
  User find(int userid);
  User find(String username);
  void update(User user);
}
