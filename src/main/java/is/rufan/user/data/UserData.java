package is.rufan.user.data;

import is.rufan.user.domain.CreditCardType;
import is.rufan.user.domain.User;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.Map;

public class UserData extends RuData implements UserDataGateway
{
  public int create(User user) throws UserDuplicateException
  {
    SimpleJdbcInsert insertUser =
        new SimpleJdbcInsert(getDataSource())
            .withTableName("users")
            .usingGeneratedKeyColumns("id");

    Map<String, Object> parameters = new HashMap<String, Object>(5);
    parameters.put("name", user.getName());
    parameters.put("username", user.getUsername());
    parameters.put("email", user.getEmail());
    parameters.put("password", user.getPassword());
    if (user.getFavoriteTeam() != null)
      parameters.put("favoriteTeamId", user.getFavoriteTeam().getTeamId());
    else
      parameters.put("favoriteTeamId", null);
      parameters.put("creditCardNumber", user.getCreditCardNumber());

    int returnKey = 0;
    try
    {
      returnKey = insertUser.executeAndReturnKey(parameters).intValue();
    }
    catch (DataIntegrityViolationException divex)
    {
      String msg = "Username is taken";
      log.warning(msg);
      throw new UserDuplicateException(msg);
    }
    return returnKey;
  }

  public User find(int userid)
  {
    String sql = "select * from users u " +
              "left join teams t on u.favoriteTeamId = t.teamId " +
              "where id = ?";
    JdbcTemplate queryPlayer = new JdbcTemplate(getDataSource());

    try
    {
      User user = queryPlayer.queryForObject(sql, new Object[]{userid},
          new UserRowMapper());
      return user;
    }
    catch(EmptyResultDataAccessException erdax)
    {
      return null;
    }
  }

  public User find(String username)
  {
    String sql = "select * from users u " +
              "left join teams t on u.favoriteTeamId = t.teamId " +
              "where username = ?";
    JdbcTemplate queryPlayer = new JdbcTemplate(getDataSource());

    try
    {
      User user = queryPlayer.queryForObject(sql, new Object[]{username},
          new UserRowMapper());
      return user;
    }
    catch(EmptyResultDataAccessException erdax)
    {
      return null;
    }
  }

  public void update(User user) {
    JdbcTemplate template = new JdbcTemplate(getDataSource());

    String sql = "update users set name = ?, email = ?, creditCardNumber = ?, " +
            "creditCardType = ?, creditCardExpMonth = ?, creditCardExpYear = ?, " +
            "favoriteTeamId = ?";

    Integer favTeamId = null;
    if (user.getFavoriteTeam() != null) {
      favTeamId = user.getFavoriteTeam().getTeamId();
    }

    Integer expMonth = null;
    Integer expYear = null;
    Integer creditCardTypeInt = -1;
    if(user.getCreditCardNumber() != null &&
        !user.getCreditCardNumber().equals("")) {
      expMonth = user.getCreditCardExpMonth();
      expYear = user.getCreditCardExpYear();
      creditCardTypeInt = user.getCreditCardType().ordinal();
    }
    sql += " where id = ?";

    template.update(sql, new Object[] { user.getName(), user.getEmail(), user.getCreditCardNumber(),
              creditCardTypeInt, expMonth, expYear, favTeamId, user.getId() });
  }
}
