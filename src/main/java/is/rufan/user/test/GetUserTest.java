package is.rufan.user.test;

import is.rufan.user.domain.User;
import is.rufan.user.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created by Snaebjorn on 10/26/2015.
 */
public class GetUserTest {
    public static void main(String[] args) {
        new GetUserTest();
    }

    public GetUserTest() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:userapp.xml");
        UserService userService = (UserService) ctx.getBean("userService");

        User user1 = userService.getUserByUsername("snaebjorn");

        System.out.println(user1);
    }
}
