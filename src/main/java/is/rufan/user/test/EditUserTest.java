package is.rufan.user.test;

import is.rufan.team.domain.Team;
import is.rufan.user.domain.CreditCardType;
import is.rufan.user.domain.User;
import is.rufan.user.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Created by Snaebjorn on 10/23/2015.
 */
public class EditUserTest {
    public static void main(String[] args) {
        new EditUserTest();
    }

    public EditUserTest() {
        // get the service
        ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:userapp.xml");
        UserService userService = (UserService) applicationContext.getBean("userService");

        User user1 = userService.getUser(1);

        System.out.println(user1);
        // System.out.println(user2);

        user1.setEmail("snaebjorn13@ru.is");
        /* user1.setCreditCardNumber("1234567890129999");
        user1.setCreditCardType(CreditCardType.VISA);
        user1.setCreditCardExpMonth(2);
        user1.setCreditCardExpYear(2017); */
        user1.setCreditCardNumber(null);
        user1.setCreditCardType(null);
        user1.setCreditCardExpMonth(0);
        user1.setCreditCardExpYear(0);
        user1.setFavoriteTeam(new Team(6140, "Liverpool", "LIV", "Liverpool", null));
        userService.updateUser(user1);

        user1 = userService.getUser(1);

        System.out.println(user1);
    }
}
