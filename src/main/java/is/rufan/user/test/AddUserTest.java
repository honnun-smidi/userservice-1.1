package is.rufan.user.test;

import is.rufan.team.domain.Team;
import is.rufan.team.domain.Venue;
import is.rufan.user.data.UserDuplicateException;
import is.rufan.user.domain.User;
import is.rufan.user.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class AddUserTest
{
  public static void main(String[] args)
  {
    new AddUserTest();
  }

  public AddUserTest()
  {
    // get the service
    ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:userapp.xml");
    UserService userService = (UserService) applicationContext.getBean("userService");

    // Add new user, a typical Liverpool fan
    User user1 = new User();
    user1.setName("Michael Shields");
    user1.setUsername("mshields");
    user1.setEmail("mshields@hotmail.com");
    user1.setPassword("anfield");
    user1.setCreditCardNumber("1234567891012345");
    user1.setFavoriteTeam(new Team(6140, "Liverpool", "LIV", "Liverpool", null));
    userService.addUser(user1);

    User user2 = new User();
    user2.setName("Baked Potato");
    user2.setUsername("bakedp");
    user2.setEmail("bakedp@hotmail.com");
    user2.setPassword("potatohead");
    user2.setCreditCardNumber("1234567891012672");
    userService.addUser(user2);

    User newUser = userService.getUser(3);

    // should throw duplicate exception
    try
    {
      userService.addUser(user1);
    }
    catch(UserDuplicateException udex)
    {
      System.out.println("Username taken!");
    }

    user1.setName("Michael Fields");

    userService.updateUser(user1);
    User updatedUser = userService.getUser(user1.getId());

    System.out.println(user1.getName());
  }
}
